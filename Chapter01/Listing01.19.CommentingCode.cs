﻿using System;

namespace EssentialCSharp8.Chapter01.Listing01_19
{
    class HeyYou
    {
        static void Main()
        {
            string firstName;   // Variable for storing the first name
            string lastName;    // Variable for storing the last name

            Console.WriteLine("Hey you!");

            Console.Write("Enter you first name: ");    /* No new line */
            firstName = Console.ReadLine();

            Console.Write("Enter you last name: ");     /* No new line */
            lastName = Console.ReadLine();

            /* Display a greeting to the console
             * using composite formatting. */
            Console.WriteLine("Your full name is {0} {1}.", firstName, lastName);

            // This is the end
            // of the program listing.
        }
    }
}
