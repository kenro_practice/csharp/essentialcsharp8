﻿using System;

namespace EssentialCSharp8.Chapter01.Listing01_15
{
    class Program
    {
        static void Main()
        {
            int readValue;
            char character;

            Console.Write("Type a character and press RETURN: ");

            readValue = Console.Read();
            character = (char)readValue;

            Console.WriteLine(character);
        }
    }
}
