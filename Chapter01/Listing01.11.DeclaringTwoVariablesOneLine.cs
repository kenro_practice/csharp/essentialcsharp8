﻿using System;

namespace EssentialCSharp8.Chapter01.Listing01_11
{
    class Program
    {
        static void Main()
        {
            string message1, message2;
            message1 = "Have fun storming the castle!";
            message2 = "Inconceivable!";
            Console.WriteLine(message1);
            Console.WriteLine(message2);
        }
    }
}
