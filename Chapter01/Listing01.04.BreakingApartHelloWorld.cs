﻿namespace EssentialCSharp8.Chapter01.Listing01_04
{
    // Class definition
    class HelloWorld
    {
        // Method declaration
        static void Main()
        {
            // Statement
            System.Console.WriteLine("Hello. my name is Inigo Montoya");
        }
    }
}
