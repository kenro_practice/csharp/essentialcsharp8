﻿using System;

namespace EssentialCSharp8.Chapter01.Listing01_17
{
    class HeyYou
    {
        static void Main()
        {
            string firstName;
            string lastName;

            Console.WriteLine("Hey you!");

            Console.Write("Enter you first name: ");
            firstName = Console.ReadLine();

            Console.Write("Enter you last name: ");
            lastName = Console.ReadLine();

            Console.WriteLine("Your full name is {0} {1}.", firstName, lastName);
        }
    }
}
