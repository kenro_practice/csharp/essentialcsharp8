﻿using System;

namespace EssentialCSharp8.Chapter01.Listing01_12
{
    class StormingTheCastle
    {
        static void Main()
        {
            string valerie;
            string miracleMax = "Have fun storming the castle!";

            valerie = "Think it will work?";

            Console.WriteLine(miracleMax);
            Console.WriteLine(valerie);

            miracleMax = "It would take a miracle.";
            Console.WriteLine(miracleMax);
        }
    }
}
