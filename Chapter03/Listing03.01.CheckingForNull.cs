﻿using System;

namespace EssentialCSharp8.Chapter03.Listing03_01
{
    class Program
    {
        static void Main()
        {
            int? number = null;

            if (number is null)
            {
                Console.WriteLine("'number' requires a value and cannot be null.");
            }
            else
            {
                Console.WriteLine($"'number' doubled is {number * 2}.");
            }
        }
    }
}