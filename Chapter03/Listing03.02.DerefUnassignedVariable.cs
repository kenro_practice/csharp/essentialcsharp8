﻿using System;

namespace EssentialCSharp8.Chapter03.Listing03_02
{
    class Program
    {
        #nullable enable
        static void Main()
        {
            // string? text;
            string? text = "This is a test";
            Console.WriteLine($"Length of text '{text}' is {text.Length}.");
        }
    }
}