﻿using System;

namespace EssentialCSharp8.Chapter03.Listing03_04
{
    class Program
    {
        static void Main()
        {
            Console.Write("Enter text: ");
            var text = Console.ReadLine();

            // Return the string in uppercase
            var uppercase = text.ToUpper();

            Console.WriteLine($"Uppercase of text '{text}' is '{uppercase}'.");
        }
    }
}