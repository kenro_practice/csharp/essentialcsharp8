﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_28
{
    class Program
    {
        static void Main()
        {
            bool boolean = true;
            string text = boolean.ToString();

            Console.WriteLine($"boolean: {boolean}");
            Console.WriteLine($"text: {text}");
        }
    }
}