﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_09
{
    class Program
    {
        static void Main()
        {
            const double number = 1.6180339887498952137893547602783409236734278;
            double result;
            string text;

            // No round trip formatting
            text = $"{number}";
            result = double.Parse(text);
            Console.WriteLine($"text:  {text}");
            Console.WriteLine($"result:  {result}");
            Console.WriteLine($"number:  {number}");
            Console.WriteLine($"result == number:  {result == number}");

            // Round trip formatting
            text = string.Format($"{number:R}");
            result = double.Parse(text);
            Console.WriteLine($"text:  {text}");
            Console.WriteLine($"result:  {result}");
            Console.WriteLine($"number:  {number}");
            Console.WriteLine($"result == number:  {result == number}");
        }
    }
}
