﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_18
{
    class Program
    {
        static void Main()
        {
            string text, uppercase;

            Console.Write("Enter text: ");
            text = Console.ReadLine();

            // Return a new string in uppercase
            uppercase = text.ToUpper();

            Console.WriteLine($"Text:  {uppercase}");
        }
    }
}