﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_19
{
    class Program
    {
        static void Main()
        {
            int? age;

            // Clear the value of age
            age = null;

            if (age == null)
            {
                Console.WriteLine($"age: null");
            }
            else
            {
                Console.WriteLine($"age: {age}");
            }
        }
    }
}