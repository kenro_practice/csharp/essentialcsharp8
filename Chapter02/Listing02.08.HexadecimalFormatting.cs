﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_08
{
    class Program
    {
        static void Main()
        {
            // Display the decimal value 42 as hexadecimal
            int num = 42;
            Console.WriteLine($"Decimal {num} is equal to hexadecimal 0x{num:X}");
        }
    }
}
