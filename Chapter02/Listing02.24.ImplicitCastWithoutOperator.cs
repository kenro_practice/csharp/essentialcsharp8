﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_24
{
    class Program
    {
        static void Main()
        {
            int intNumber = 31416;
            long longNumber = intNumber;
            
            Console.WriteLine($"intNumber: {intNumber}");
            Console.WriteLine($"longNumber: {longNumber}");
        }
    }
}