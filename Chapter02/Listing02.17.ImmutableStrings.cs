﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_17
{
    class Program
    {
        static void Main()
        {
            string text;

            Console.Write("Enter text: ");
            text = Console.ReadLine();

            // UNEXPECTED:  Does not conver text to uppercase
            text.ToUpper();

            Console.WriteLine($"Text:  {text}");
        }
    }
}