﻿using static System.Console;

namespace EssentialCSharp8.Chapter02.Listing02_15
{
    class Program
    {
        static void Main()
        {
            string firstName, lastName;

            WriteLine("Hey you!");

            Write("Enter your first name: ");
            firstName = ReadLine();

            Write("Enter your last name: ");
            lastName = ReadLine();

            WriteLine($"Your full name is {firstName} {lastName}.");
        }
    }
}