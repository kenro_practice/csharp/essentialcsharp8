﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_16
{
    class Program
    {
        static void Main()
        {
            string palindrome;

            Console.Write("Enter a palindrome: ");
            palindrome = Console.ReadLine();

            Console.WriteLine(
                $"The palindrome \"{palindrome}\" is {palindrome.Length} characters.");
        }
    }
}