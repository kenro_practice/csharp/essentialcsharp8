﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_14
{
    class Program
    {
        static void Main()
        {
            // Display a triangle using a verbatim string literal
            Console.WriteLine(@"begin
                    /\
                   /  \
                  /    \
                 /      \
                /________\
end");
        }
    }
}