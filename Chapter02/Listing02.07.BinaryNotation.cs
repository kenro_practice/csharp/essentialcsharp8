﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_07
{
    class Program
    {
        static void Main()
        {
            // Display the decimal value 42 using a binary literal
            Console.WriteLine(0b_101010);
        }
    }
}
