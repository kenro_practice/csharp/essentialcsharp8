﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_10
{
    class Program
    {
        static void Main()
        {
            string option = "/help";
            string optionCompare = "/Help";
            int comparison = string.Compare(option, optionCompare, true);

            Console.WriteLine($"option: {option}");
            Console.WriteLine($"optionCompare: {optionCompare}");
            Console.WriteLine($"Case-insensitive comparison: {comparison}");
        }
    }
}
