﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_25
{
    class Program
    {
        static void Main()
        {
            int intNumber = 31416;
            long longNumber = (long) intNumber;

            Console.WriteLine($"intNumber: {intNumber}");
            Console.WriteLine($"longNumber: {longNumber}");
        }
    }
}