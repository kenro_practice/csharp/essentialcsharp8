﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_26
{
    class Program
    {
        static void Main()
        {
            string text = "9.11E-31";
            float kgElectronMass = float.Parse(text);

            Console.WriteLine($"text: {text}");
            Console.WriteLine($"kgElectronMass: {kgElectronMass}");
        }
    }
}