﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_30
{
    class Program
    {
        static void Main()
        {
            // double number;
            string input;

            Console.Write("Enter a number: ");
            input = Console.ReadLine();

            if (double.TryParse(input, out double number))
            {
                Console.WriteLine($"Input was parsed successfully to: {number}");
            }
            else
            {
                Console.WriteLine("The text entered was not a valid number");
            }
        }
    }
}