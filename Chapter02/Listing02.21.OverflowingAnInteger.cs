﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_21
{
    class Program
    {
        static void Main()
        {
            // Set an integer to the maximum value and perform an overflow operation
            var n = int.MaxValue;
            Console.WriteLine($"int.MaxVal: {n}");
            n += 1;
            Console.WriteLine($"int.MaxVal+1: {n}");
        }
    }
}