﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_12
{
    class Program
    {
        static void Main()
        {
            // Display a smiley face using Unicode encoding
            Console.Write('\u003A');
            Console.WriteLine('\u0029');
        }
    }
}
