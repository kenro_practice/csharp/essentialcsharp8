﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_06
{
    class Program
    {
        static void Main()
        {
            // Display the decimal value 42 using a hexadecimal literal
            Console.WriteLine(0x_002A);
        }
    }
}
