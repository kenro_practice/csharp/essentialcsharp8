﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_13
{
    class Program
    {
        static void Main()
        {
            // Inserting newline character in strings
            Console.Write("\"Truly, you have a dizzying intellect.\"");
            Console.Write("\n\"Wait 'til I get going!\"\n");
        }
    }
}
