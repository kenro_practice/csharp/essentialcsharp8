﻿using System;

namespace EssentialCSharp8.Chapter02.Listing02_27
{
    class Program
    {
        static void Main()
        {
            string middleCText = "261.626";
            double middleC = Convert.ToDouble(middleCText);
            bool boolean = Convert.ToBoolean(middleC);

            Console.WriteLine($"middleCText: {middleCText}");
            Console.WriteLine($"middleC: {middleC}");
            Console.WriteLine($"boolean: {boolean}");
        }
    }
}